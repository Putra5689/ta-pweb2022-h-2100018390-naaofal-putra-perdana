<?php

$nilai = 95.00;

if ($nilai > 80.00) {
    $grade = "A";
} elseif($nilai > 76.25){
    $grade = "A-";
} elseif($nilai > 68.75){
    $grade = "B+";
} elseif($nilai > 65.00){
    $grade = "B";
} elseif($nilai > 62.50){
    $grade = "B-";
} elseif($nilai > 57.50){
    $grade = "C+";
} elseif($nilai > 55.00){
    $grade = "C";
} elseif($nilai > 51.25){
    $grade = "C-";
} elseif($nilai > 43.75){
    $grade = "D+";
} elseif($nilai > 40.00){
    $grade = "D";
} else {
    $grade = "E";
}

echo "Nilai anda: $nilai<br>";
echo "Kategori: $grade";

?>